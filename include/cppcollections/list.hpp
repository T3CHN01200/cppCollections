#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <utility>

#include "heap.hpp"
#include "util.hpp"

#pragma once

template <typename T>
struct List
{
	T *array;
	size_t length;
	size_t capacity;
	void*(*malloc)(size_t);
	void*(*realloc)(void*, size_t);
	void(*free)(void*);
};

template <typename T>
List<T> newList()
{
	List<T> ret = {
		.array = (T*)malloc(sizeof(T)),
		.length = 0,
		.capacity = 1,
		.malloc = &malloc,
		.realloc = &realloc,
		.free = &free,
	};
	return ret;
}

template <typename T>
List<T> newList(size_t capacity)
{
	List<T> ret = {
		.array = (T*)malloc(sizeof(T) * capacity),
		.length = 0,
		.capacity = capacity,
		.malloc = &malloc,
		.realloc = &realloc,
		.free = &free,
	};
	return ret;
}

template <typename T>
List<T> newList(
	size_t capacity,
	void*(*customMalloc)(size_t),
	void*(*customRealloc)(void*, size_t),
	void(customFree)(void*)
)
{
	List<T> ret = {
		.array = (T*)customMalloc(sizeof(T)),
		.length = 0,
		.capacity = 1,
		.malloc = customMalloc,
		.realloc = customRealloc,
		.free = customFree,
	};
	return ret;
}

template <typename T>
List<T> newList(
	size_t count,
	T *handle
)
{
	List<T> ret = {
		.array = handle,
		.length = count,
		.capacity = count,
		.malloc = &malloc,
		.realloc = &realloc,
		.free = &free,
	};
	return ret;
}

template <typename T>
List<T> newList(
	size_t count,
	T *handle,
	void*(*customMalloc)(size_t),
	void*(*customRealloc)(void*, size_t),
	void(customFree)(void*)
)
{
	List<T> ret = {
		.array = handle,
		.length = count,
		.capacity = count,
		.malloc = customMalloc,
		.realloc = customRealloc,
		.free = customFree,
	};
	return ret;
}

template <typename T>
void add(List<T> *list, T item)
{
	if (list->length == list->capacity)
	{
		list->capacity *= 2;
		list->array = (T*)list->realloc(
			list->array,
			list->capacity * sizeof(T)
		);
	}
	*(list->array + list->length) = item;
	list->length++;
}

template <typename T>
void addRange(List<T> *list, size_t count, T items[count])
{
	while ((list->capacity - list->length) < count) list->capacity *= 2;
	list->array = (T*)list->realloc(list->array, list->capacity * sizeof(T));
	memmove(list->array + list->length, items, count * sizeof(T));
	list->length += count;
}

template <typename T>
uint8_t remove(List<T> *list, T item)
{	
	uint8_t shouldRemove = 1;
	uint8_t* a = (uint8_t*) &item;
	uint8_t* b;
	for (size_t i = 0; i < list->length; i++)
	{
		b = (uint8_t*) (list->array + i);
		for (size_t j = 0; j < sizeof(T); j++)
		{
			shouldRemove &= *(a) == *(b);
			a++;
			b++;
		}
		if (shouldRemove)
		{
			T* dest = list->array + i;
			size_t count = (list->capacity - i) * sizeof(T);
			memmove(dest, dest + 1, count);
			list->length--;
			return 1;
		}
		else
		{
			shouldRemove = 1;
			a = (uint8_t*) &item;
		}
	}
	return 0;
}

template <typename T>
uint8_t remove(
	List<T> *list,
	T item,
	void* state,
	void(*deleter)(void*, T*)
)
{	
	uint8_t shouldRemove = 1;
	uint8_t* a = (uint8_t*) &item;
	uint8_t* b;
	for (size_t i = 0; i < list->length; i++)
	{
		b = (uint8_t*) (list->array + i);
		for (size_t j = 0; j < sizeof(T); j++)
		{
			shouldRemove &= *(a) == *(b);
			a++;
			b++;
		}
		if (shouldRemove)
		{
			T* dest = list->array + i;
			deleter(state, dest);
			size_t count = (list->capacity - i) * sizeof(T);
			memmove(dest, dest + 1, count);
			list->length--;
			return 1;
		}
		else
		{
			shouldRemove = 1;
			a = (uint8_t*) &item;
		}
	}
	return 0;
}

template <typename T>
uint8_t removeAt(List<T> *list, size_t index)
{
	if (index < list->length)
	{
		T* dest = list->array + index;
		size_t count = (list->capacity - index) * sizeof(T);
		memmove(dest, dest + 1, count);
		list->length--;
		return 1;
	}
	return 0;
}

template <typename T>
uint8_t removeAt(
	List<T> *list,
	size_t index,
	void* state,
	void(*deleter)(void*, T*)
)
{
	if (index < list->length)
	{
		T* dest = list->array + index;
		deleter(state, dest);
		size_t count = (list->capacity - index) * sizeof(T);
		memmove(dest, dest + 1, count);
		list->length--;
		return 1;
	}
	return 0;
}

template <typename T>
std::pair<size_t, uint8_t> indexOf(List<T> *list, T item)
{
	uint8_t isMatch = 1;
	uint8_t* a = (uint8_t*) &item;
	uint8_t* b;
	for (size_t i = 0; i < list->length; i++)
	{
		b = (uint8_t*) (list->array + i);
		for (size_t j = 0; j < sizeof(T); j++)
		{
			isMatch &= *(a) == *(b);
			a++;
			b++;
		}
		if (isMatch)
		{
			std::pair<size_t, uint8_t> ret = { i, 1 };
			return ret;
		}
		else
		{
			isMatch = 1;
			a = (uint8_t*) &item;
		}
	}
	std::pair<size_t, uint8_t> ret = { 0, 0 };
	return ret;
}

template <typename T>
std::pair<size_t, uint8_t> indexOf(
	List<T> *list,
	T item,
	int8_t(*comparer)(const T*, const T*)
)
{
	uint8_t isMatch = 1;
	for (size_t i = 0; i < list->length; i++)
	{
		isMatch &= comparer(list->array + i, &item);
		if (isMatch)
		{
			std::pair<size_t, uint8_t> ret = { i, 1 };
			return ret;
		}
	}
	std::pair<size_t, uint8_t> ret = { 0, 0 };
	return ret;
}

template <typename T>
std::pair<size_t, uint8_t> lastIndexOf(List<T> *list, T item)
{
	uint8_t isMatch = 1;
	uint8_t* a = (uint8_t*) &item;
	uint8_t* b;
	for (size_t i = 0; i < list->length; i++)
	{
		b = (uint8_t*) (list->array + i);
		for (size_t j = 0; j < sizeof(T); j++)
		{
			isMatch &= *(a) == *(b);
			a++;
			b++;
		}
		if (isMatch)
		{
			std::pair<size_t, uint8_t> ret = { i, 1 };
			return ret;
		}
		else
		{
			isMatch = 1;
			a = (uint8_t*) &item;
		}
	}
	std::pair<size_t, uint8_t> ret = { 0, 0 };
	return ret;
}

template <typename T>
void clear(List<T> *list)
{
	memset(list->array, 0, list->capacity * sizeof(T));
	list->length = 0;
}

template <typename T>
void clear(
	List<T> *list,
	void *state,
	void(*deleter)(void*, T*)
)
{
	T* endPtr = list->array + list->length;
	for (T* p = list->array; p < endPtr; ++p)
	{
		deleter(state, p);
	}
	memset(list->array, 0, list->capacity * sizeof(T));
	list->length = 0;
}

template <typename T>
void insert(List<T> *list, size_t index, T item)
{
	size_t minLength = (list->length + 1);
	while(
		list->capacity < minLength &
		list->capacity <= index) list->capacity *= 2;
	list->array = (T*)list->realloc(list->array, list->capacity * sizeof(T));
	memmove(
		list->array + index,
		list->array + index + 1,
		list->capacity - list->length
	);
	*(list->array + index) = item;
}

template <typename T>
void insertRange(
	const List<T> *list,
	const size_t index,
	const size_t count,
	const T items[count]
)
{
	while(
		list->capacity < (list->length + count) &
		list->capacity <= (index + count - 1)) list->capacity *= 2;
	list->array = (T*)list->realloc(list->array, list->capacity * sizeof(T));
	memmove(
		list->array + index,
		list->array + index + count,
		list->capacity - list->length
	);
	memmove(
		list->array + index,
		items,
		count * sizeof(T)
	);
}

template <typename T>
void forEach(List<T> *list, void *state, void(*action)(void*, T*))
{
	T* endPtr = list->array + list->length;
	for (T* p = list->array; p < endPtr; ++p)
	{
		action(state, p);
	}
}

template <typename T>
void freeList(List<T> *list)
{
	if (list->array)
		free(list->array);
	list->array = 0;
}

template <typename T>
void freeList(
	List<T> *list,
	void *state,
	void(*deleter)(void*, T*)
)
{
	T* endPtr = list->array + list->length;
	for (T* p = list->array; p < endPtr; ++p)
	{
		deleter(state, p);
	}
	if (list->array)
		list->free(list->array);
	list->array = 0;
}

template <typename T>
void clear(
	List<T> *list,
	void *state,
	void(*deleter)(void*, T)
)
{
	T* endPtr = list->array + list->length;
	for (T* p = list->array; p < endPtr; ++p)
	{
		deleter(state, p);
	}
	memset(list->array, 0, list->capacity * sizeof(T));
	list->length = 0;
}

template <typename T>
uint8_t set(List<T> *list, T item, size_t index)
{
	if (index < list->length)
	{
		*(list->array + index) = item;
		return 1;
	}
	return 0;
}

template <typename T>
std::pair<T, uint8_t> get(List<T> *list, size_t index)
{
	std::pair<T, uint8_t> ret = { 0 };
	if (index < list->length)
	{
		ret.first = *(list->array + index);
		ret.second = 1;
	}
	return ret;
}

template <typename T>
std::pair<T*, uint8_t> getRef(List<T> *list, size_t index)
{
	uint8_t inRange = index < list->length;
	std::pair<T*, uint8_t> ret = { list->array + index, inRange };
	return ret;
}

template <typename T>
void sort(
	List<T> *list,
	int8_t(*comparer)(const T*, const T*)
)
{
	Heap<T> heap = {
		.array = list->array,
		.length = list->length,
		.capacity = list->capacity,
		.comparer = comparer,
		.malloc = list->malloc,
		.realloc = list->realloc,
		.free = list->free,
	};
	heapify(&heap);
	T tmp;
	size_t counter = 0;
	while (heap.length)
	{
		tmp = pop(&heap);
		*(list->array + heap.length) = tmp;
	}
	if (comparer(list->array, list->array + 1) > 0)
		swap(list->array, list->array + 1);
}

template <typename T>
std::pair<size_t, uint8_t> binarySearch(
	List<T> *list,
	T item,
	int8_t(*comparer)(const T*, const T*)
)
{
	size_t hi = list->length - 1;
	size_t lo = 0;
	size_t mid = 0;
	std::pair<size_t, uint8_t> ret = { 0, 0 };
	int8_t cmp = 0;
	while ((hi - lo) < list->length)
	{
		mid = (hi + lo) / 2;
		cmp = comparer(list->array + mid, &item);
		if (cmp == 0)
		{
			ret.first = mid;
			ret.second = 1;
			break;
		}
		if (cmp < 0)
			lo = mid + 1;
		else
			hi = mid - 1;
	}
	return ret;
}
