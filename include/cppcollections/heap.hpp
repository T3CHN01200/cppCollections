#include <cstddef>
#include <cstdint>
#include <cstdlib>

#include "util.hpp"

#pragma once

template <typename T>
struct Heap
{
	T *array;
	size_t length;
	size_t capacity;
	int8_t(*comparer)(const T*, const T*);
	void*(*malloc)(size_t);
	void*(*realloc)(void*, size_t);
	void(*free)(void*);
};

template <typename T>
Heap<T> newHeap(int8_t(*comparer)(const T*, const T*))
{
	Heap<T> ret = {
		.array = malloc(sizeof(T)),
		.length = 0,
		.capacity = 1,
		.comparer = comparer,
		.malloc = &malloc,
		.realloc = &realloc,
		.free = &free,
	};
	return ret;
}

template <typename T>
Heap<T> newHeap(size_t capacity, int8_t(*comparer)(const T*, const T*))
{
	Heap<T> ret = {
		.array = malloc(sizeof(T) * capacity),
		.length = 0,
		.capacity = capacity,
		.comparer = comparer,
		.malloc = &malloc,
		.realloc = &realloc,
		.free = &free,
	};
	return ret;
}

template <typename T>
Heap<T> newHeap(
	size_t capacity,
	int8_t(*comparer)(const T*, const T*),
	void*(*customMalloc)(size_t),
	void*(*customRealloc)(void*, size_t),
	void(*customFree)(void*)
)
{
	Heap<T> ret = {
		.array = customMalloc(sizeof(T) * capacity),
		.length = 0,
		.capacity = capacity,
		.comparer = comparer,
		.malloc = &customMalloc,
		.realloc = &customRealloc,
		.free = &customFree,
	};
	return ret;
}

template <typename T>
Heap<T> newHeap(
	size_t count,
	T* handle,
	int8_t(*comparer)(const T*, const T*)
)
{
	Heap<T> ret = {
		.array = malloc(sizeof(T) * count),
		.length = count,
		.capacity = count,
		.comparer = comparer,
		.malloc = &malloc,
		.realloc = &realloc,
		.free = &free,
	};
	return ret;
}

template <typename T>
Heap<T> newHeap(
	size_t count,
	T *handle,
	int8_t(*comparer)(const T*, const T*),
	void*(*customMalloc)(size_t),
	void*(*customRealloc)(void*, size_t),
	void(*customFree)(void*)
)
{
	Heap<T> ret = {
		.array = customMalloc(sizeof(T) * count),
		.length = count,
		.capacity = count,
		.comparer = comparer,
		.malloc = customMalloc,
		.realloc = customRealloc,
		.free = customFree,
	};
	return ret;
}

template <typename T>
void heapify(Heap<T> *heap)
{
	size_t largest = 0;
	size_t index;
	size_t left;
	size_t right;
	size_t end = (heap->length/2) + 1;
	for (size_t i = 0; i <= end; ++i)
	{
		largest = end - i;
		do
		{
			index = largest;
			largest = index;
			left = 2 * index + 1;
			right = 2 * index + 2;
			if (left < heap->length &&
				heap->comparer(heap->array + left,
				heap->array + largest) > 0)
				largest = left;
			if (left < heap->length &&
				heap->comparer(heap->array + right,
				heap->array + largest) > 0)
				largest = right;
			swap(heap->array + index, heap->array + largest);
		}
		while (index != largest & index >= 0);
	}
}

template <typename T>
T peek(Heap<T> *heap)
{
	return *(heap->array);
}

template <typename T>
T pop(Heap<T> *heap)
{
	size_t index = 0;
	T root = *(heap->array);
	heap->length--;
	T* ptr = heap->array;
	*(ptr) = *(heap->array + heap->length);
	T* a = heap->array + ((2 * index) + 1);
	T* b = heap->array + ((2 * index) + 2);
	while (
		(a < heap->array + heap->length) &
		(b < heap->array + heap->length) &&
		(heap->comparer(ptr, a) < 0 |
		heap->comparer(ptr, b) < 0)
	)
	{
		index = (2 * index);
		if (heap->comparer(a, b) > 0)\
		{
			swap(ptr, a);
			index = index + 1;
		}
		else
		{
			swap(ptr, b);
			index = index + 2;
		}
		ptr = heap->array + index;
		a = heap->array + ((2 * index) + 1);
		b = heap->array + ((2 * index) + 2);
	}
	return root;
}

template <typename T>
void push(Heap<T> *heap, T item)
{
	if (heap->length == heap->capacity)
	{
		heap->capacity *= 2;
		heap->array = realloc(heap->array, heap->capacity * sizeof(T));
	}
	*(heap->array + heap->length) = item;
	size_t index = heap->length;
	heap->length++;
	while (
		heap->comparer(
			heap->array + index,
			heap->array + ((index - 1) / 2)
		) > 0 &
		index != 0
	)
	{
		swap(heap->array + index, heap->array + ((index - 1) / 2));
		index = (index - 1) / 2;
	}
}
