#pragma once

template <typename T>
constexpr void swap(T *a, T *b)
{
	T tmp = *a;
	*a = *b;
	*b = tmp;
}
